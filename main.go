package main

import (
	"image/jpeg"
	"log"
	"marching_cubes/mc"
	"os"
)

func main(){
	vol := mc.Volume{}

	//TODO para cada imagem, criar um frame e adicionar ao volume
	dir, _ := os.Getwd()
	root := dir + "\\resources\\black-white"

	decoder := mc.NewDecoder(mc.RepositoryConfig{
		Root: root, Width:  250, Height: 250,
	})

	files := decoder.GetSortFiles()
	for _ ,f := range files {
		file, err := os.Open(f)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
		jpg, err := jpeg.Decode(file)
		if err != nil {
			log.Fatal(err)
		}
		frame := mc.NewFrame(jpg)
		//frame.SaveBlackWhite(strings.ReplaceAll(f,"C:\\Users\\gusta\\go\\src\\marching_cubes\\resources\\ARTERIELLE_6168\\", ""))
		frame.AddToVolume(&vol)
	}

	triangles := mc.NewMarchingCubes().Marching(vol)

	//TODO salvar triangles em um arquivo para poder mostrar em outra ferramenta
	out, err := os.Create("resources/output.txt")
	if err != nil {
		log.Fatal(err.Error())
	}
	defer out.Close()
	for _,t := range triangles {
		out.Write([]byte(t.String() + "\n"))
	}
}
