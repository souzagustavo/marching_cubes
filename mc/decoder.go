package mc

import (
	"io/ioutil"
	"log"
	"sort"
)

type RepositoryConfig struct {
	Root string
	Width, Height int
}

type Decoder struct {
	Rep RepositoryConfig
}

func NewDecoder(rep RepositoryConfig) *Decoder {
	return &Decoder{rep}
}

func (d Decoder)  GetSortFiles() []string {
	fileInfo, err := ioutil.ReadDir(d.Rep.Root)
	if err != nil {
		log.Fatal(err)
	}
	var sorted []string
	for _, file := range fileInfo {
		sorted = append(sorted, d.Rep.Root + "\\"+file.Name())
	}
	sort.Strings(sorted)
	return sorted
}


