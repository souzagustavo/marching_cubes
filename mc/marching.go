package mc

import "log"

const CUBE_SIZE = 5

type MarchingCubes struct {
	cube 		*Cube
	threshold 	uint16
}

func NewMarchingCubes() *MarchingCubes{
	return &MarchingCubes{}
}

//Um Cubo 3x3x3 percorre o volume já populado, calculando os triângulos.
//Retorna lista de triângulos que compoõem o volume completo
func (mc *MarchingCubes) Marching(vol Volume) []Triangle{
	var triangles []Triangle
	mc.cube = NewCube(CUBE_SIZE)

	mx, my, mz := vol.GetBounds()
	mx, my, mz = mx-CUBE_SIZE, my-CUBE_SIZE, mz-CUBE_SIZE
	next := CUBE_SIZE-1
	log.Printf("Volume dimension: x: %d, y:%d, z:%d", mx, my, mz)
	for z := 0; z < mz; z+=next{ //itera frames
		for x := 0; x < mx; x+=next {
			for y := 0; y < my; y+=next{
				mc.SetCube(vol,x,y,z)
				triangles = append(triangles, mc.cube.GetTriangles()...)
				mc.cube.Reset()
			}
		}
	}
	return triangles
}

func (mc *MarchingCubes) SetCube(vol Volume, x,y,z int){
	vol.SetVertice(0, x, y, z+(CUBE_SIZE-1), mc.cube)
	vol.SetVertice(1, x+(CUBE_SIZE-1), y, z+(CUBE_SIZE-1), mc.cube)
	vol.SetVertice(2, x+(CUBE_SIZE-1), y+(CUBE_SIZE-1), z+(CUBE_SIZE-1), mc.cube)
	vol.SetVertice(3, x, y+(CUBE_SIZE-1), z+(CUBE_SIZE-1), mc.cube)
	vol.SetVertice(4, x, y, z, mc.cube)
	vol.SetVertice(5, x+(CUBE_SIZE-1), y, z, mc.cube)
	vol.SetVertice(6, x+(CUBE_SIZE-1), y+(CUBE_SIZE-1), z, mc.cube)
	vol.SetVertice(7, x, y+(CUBE_SIZE-1), z, mc.cube)

	mid := int(CUBE_SIZE / 2)
	mc.cube.Edges[0] = Point{x+mid, y, z+(CUBE_SIZE-1)}
	mc.cube.Edges[1] = Point{x+(CUBE_SIZE-1), y+mid, z+(CUBE_SIZE-1)}
	mc.cube.Edges[2] = Point{ x+mid, y+(CUBE_SIZE-1), z+(CUBE_SIZE-1)}
	mc.cube.Edges[3] = Point{x, y+mid, z+(CUBE_SIZE-1)}
	mc.cube.Edges[4] = Point{x+mid, y, z}
	mc.cube.Edges[5] = Point{x+(CUBE_SIZE-1), y+mid, z}
	mc.cube.Edges[6] = Point{x+mid, y+(CUBE_SIZE-1), z}
	mc.cube.Edges[7] = Point{x, y+mid, z}
	mc.cube.Edges[8] = Point{x, y, z+mid	}
	mc.cube.Edges[9] = Point{x+(CUBE_SIZE-1), y, z+mid}
	mc.cube.Edges[10] = Point{x+(CUBE_SIZE-1), y+(CUBE_SIZE-1),z+mid}
	mc.cube.Edges[11] = Point{x, y+(CUBE_SIZE-1),z+mid}

}
