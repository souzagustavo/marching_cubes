package mc

import (
	"github.com/nfnt/resize"
	"image"
	"image/color"
	"image/jpeg"
	"log"
	"os"
)

type Frame [][]bool

func newFrame(x, y int) Frame {
	frame := make([][]bool, x)
	for index,_ := range frame {
		frame[index] = make([]bool, y)
	}
	return frame
}


func NewFrame(img image.Image) Frame {
	var threshold = 252
	var gray int
	var r,g,b uint32
	bounds := img.Bounds()

	frame := newFrame(bounds.Max.X, bounds.Max.Y)
	for y := 0; y < bounds.Max.Y; y++{
		for x := 0; x < bounds.Max.X; x++{
			r,g,b,_ = img.At(x,y).RGBA()
			gray = int((0.299*float64(r) + 0.587*float64(g) + 0.114*float64(b)) / 256)
			if gray > threshold {
				frame[x][y] = true
			} else {
				frame[x][y] = false
			}
		}
	}
	return frame
}

func (f Frame) SaveBlackWhite(filename string){
	img := image.NewRGBA(image.Rect(0,0,512,512))

	// Set color for each pixel.
	for x := 0; x < 512; x++ {
		for y := 0; y < 512; y++ {
			var c color.RGBA
			if f[x][y] {
				c = color.RGBA{255,255,255,255}
			} else {
				c = color.RGBA{0,0,0,255}
			}
			img.Set(x,y, c)
		}
	}

	// resize to width 1000 using Lanczos resampling
	// and preserve aspect ratio
	m := resize.Resize(250, 0, img, resize.Lanczos3)

	// Encode as PNG.
	out, err := os.Create("resources/black-white/" + filename+ ".jpg")
	if err != nil {
		log.Fatal(err.Error())
	}
	err = jpeg.Encode(out, m, nil)
	if err != nil {
		log.Fatal(err.Error())
	}
}

func (f Frame) AddToVolume(v *Volume){
	*v = append(*v, f)
}