package mc

type Vertice struct {
	//Point Point
	Value bool
}

type Cube struct {
	Dim int
	Vertices 	map[int]Vertice
	Edges		map[int]Point
}

//Cria um cubo com uma dimensão específica
func NewCube(dim int) *Cube {
	return &Cube{
		Dim: dim,
		Vertices: make(map[int]Vertice),
		Edges: make(map[int]Point),
	}
}

//Limpa o cubo.
func (c *Cube) Reset() {
	c.Vertices = make(map[int]Vertice)
	c.Edges = make(map[int]Point)
}

//Calcula o indice da tabela de triangulos com base no valor dos vértices.
//Retorna lista de triangulos que compõem o cubo. Cada triângulo contem suas coordenadas.
func (c *Cube) GetTriangles() []Triangle{
	var cubeIndex uint8 = 0
	if c.Vertices[0].Value{
		cubeIndex |= 1
	}
	if c.Vertices[1].Value{
		cubeIndex |= 2
	}
	if c.Vertices[2].Value {
		cubeIndex |= 4
	}
	if c.Vertices[3].Value {
		cubeIndex |= 8
	}
	if c.Vertices[4].Value {
		cubeIndex |= 16
	}
	if c.Vertices[5].Value {
		cubeIndex |= 32
	}
	if c.Vertices[6].Value {
		cubeIndex |= 64
	}
	if c.Vertices[7].Value {
		cubeIndex |= 128
	}
	return c.getTrianglePoints(TriangleTable[cubeIndex])
}

// Entrada: vetor triangleTable.
// A cada 3 posições do vetor, um triangle é formado.
// Retorno: lista das coordenadas do triângulo
func (c *Cube) getTrianglePoints(triTable []int) []Triangle{
	var triangles []Triangle

	for i := 0; i < len(triTable)-3 ; i+=3{
		t := Triangle{
			Points: [3]Point{
				c.Edges[triTable[i]],
				c.Edges[triTable[i+1]],
				c.Edges[triTable[i+2]],
		}}
		triangles = append(triangles, t)
	}
	return triangles
}