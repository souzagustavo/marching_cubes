package mc

import "log"

//O volume é uma estrutura 3D em preto (false) e branco (true)
type Volume [][][]bool

//Retorna as dimensões do cubo x,y,z
func (v Volume) GetBounds() (int,int,int) {
	z := len(v)
	if z == 0 {
		log.Fatal("invalid z bound")
	}
	x := len(v[0])
	if x == 0 {
		log.Fatal("invalid x bound")
	}
	y := len(v[0][0])
	if y == 0 {
		log.Fatal("invalid y bound")
	}
	return x, y, z
}

//Popula o vértice correspondente ao index do cubo
func (v Volume) SetVertice(index, x, y, z int, cube *Cube){
	cube.Vertices[index] = Vertice{
		Value: v[z][x][y],
	}
}